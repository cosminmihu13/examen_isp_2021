package subiect2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Graph extends JFrame implements ActionListener {
    private JTextArea textArea;
    private JTextArea textArea1;
    private JButton button;

    public Graph() {
        this.setTitle("Interface");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        init();
        button.addActionListener(this);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Graph();
    }

    public void init() {
        this.setLayout(null);
        textArea = new JTextArea();
        textArea.setBounds(70, 40, 150, 20);

        textArea1 = new JTextArea();
        textArea1.setBounds(70, 80, 150, 20);
        textArea1.setEditable(false);

        button = new JButton();
        button.setText("Button");
        button.setBounds(105, 120, 80, 20);

        add(textArea);
        add(textArea1);
        add(button);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            if (!textArea.getText().isEmpty()) {
                String r = "a";
                int n = textArea.getText().length();
                textArea1.setText(r.repeat(n));
            } else {
                JOptionPane.showMessageDialog(this,
                        "Text Area empty",
                        "Warning",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}
