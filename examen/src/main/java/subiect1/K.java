package subiect1;

public class K extends S {
    private L l;

    public K() {
        l = new L();
    }
}

class S {
    public void metB() {

    }
}

class L {
    public void metA() {

    }
}

class I {
    private long t;
    private K k;

    public void f() {

    }

    public void i(J j) {

    }
}

class J {

}

class N {
    private I i;

    public N(I i) {
        this.i = i;
    }
}